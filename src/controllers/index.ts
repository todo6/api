import AuthController from './AuthController';
import UsersController from './UsersController';

export default [
  AuthController,
  UsersController,
];
