"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const boom_1 = __importDefault(require("@hapi/boom"));
const cleanErrors = (errors) => (errors.map(({ message, path, value }) => {
    return { message, field: path, value };
}));
// eslint-disable-next-line no-unused-vars,@typescript-eslint/no-unused-vars
exports.default = (e, req, res, next) => {
    if (!req.headers['x-test']) {
        console.log('ErrorHandler', { e });
    }
    if (boom_1.default.isBoom(e)) {
        return res
            .status(e.output.statusCode)
            .json(Object.assign(Object.assign({}, e.output.payload), { data: e.data }));
    }
    if (e.name === 'SequelizeValidationError') {
        return res.status(422).json({
            statusCode: 422,
            error: 'Unprocessable Entity',
            message: 'Errores de validación',
            validationErrors: cleanErrors(e.errors),
        });
    }
    if (process.env.NODE_ENV !== 'production') {
        return res.status(500).json({
            statusCode: 500,
            error: e.name,
            message: e.message,
            stack: e.stack ? e.stack.split('\n') : null,
            sql: e.sql,
        });
    }
    return res.status(500).json({
        statusCode: 500,
        error: 'Internal Server Error',
        message: 'An unknown error occurred on the server',
    });
};
//# sourceMappingURL=errorHandler.js.map