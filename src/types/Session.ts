import { UserInstance } from '../models/User';

export type Session = {
  token: string;
  user: UserInstance;
}