import Boom from '@hapi/boom';
import moment, { duration } from 'moment';
import dotenv from 'dotenv';
import mongoose, { Schema, Document, Model } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import { resolve } from 'path';
import jwt from 'jsonwebtoken';
import ms from 'ms';
import { Session } from '../types/Session';
import { AppModel } from '../types/AppModel';
import { checkPassword } from '../utils/crypto';
import { AuthTokens } from '../types/AuthTokens';

dotenv.config();

export interface Task {
  _id?: string;
  title: string;
  description: string;
  check?: boolean;
}
interface UserAttributes extends Document {
  readonly _id: number;
  name: string;
  lastName: string;
  email: string;
  password?: string;
  enabled: boolean;
  confirmed: boolean;
  checklist: Task[];
  // timestamps!
  readonly createdAt?: Date | string | moment.Moment;
  updatedAt?: Date | string | moment.Moment;
}

export interface User extends UserAttributes{}
const schemaChecklist: Schema = new Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: false,
    default: '',
  },
  check: {
    type: Boolean,
    required: true,
    default: false,
  },
});
schemaChecklist.plugin(uniqueValidator);
const schemaUser: Schema = new Schema({
  name: String,
  lastName: String,
  email: {
    type: String,
    lowercase: true,
    required: true,
    unique: true,
  },
  password: String,
  enabled: {
    type: Boolean,
    default: true,
  },
  confirmed: {
    type: Boolean,
    default: true,
    // IMPORTANTE: En esta versión de demostración
    // no se ocupa confirmar el correo electrónico
  },
  checklist: [schemaChecklist],
  createdAt: { type: Date, default: moment().format() },
  updatedAt: { type: Date, default: moment().format() },
});

schemaUser.plugin(uniqueValidator);




schemaUser.virtual('fullName').get(function() {
  const { name, lastName } = this;
  return name + ' ' + lastName;
});

export interface UserInstance extends UserAttributes {
  fullName: string;
}

schemaUser.methods.login = async function(
  email: string,
  password: string
): Promise<UserInstance> {
  const user = await User.findOne({ email });
  if (!user) throw Boom.conflict('El email ingresado no esta registrado');
  if (!user.enabled) throw Boom.conflict('Usuario inactivo');
  if (!user.confirmed) throw Boom.conflict('Primero debe confirmar su correo');
  if (!(await checkPassword(password, user.password))) {
    throw Boom.conflict('Contraseña incorrecta');
  }
  return user;
}

// TODO: Session type pendiente
schemaUser.methods.getSession = async (user: UserInstance): Promise<Session> => {
  const { _id, email } = user;
  const payload = await User.findOne({ _id });
  const token = jwt.sign(
    JSON.stringify({
      _id,
      email,
    }),
    process.env.API_JWT_SECRET,
  );

  return { token, user: payload };

}

const User = mongoose.model<UserInstance>('User', schemaUser);
User.prototype.toJSON = function toJSON() {
  const result = this.toObject();
  delete result.password;
  delete result.__v;
  return result;
};
export default User;
