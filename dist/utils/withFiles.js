"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable no-param-reassign, no-prototype-builtins, no-lonely-if, no-underscore-dangle */
const lodash_1 = require("lodash");
const getFolderFiles_1 = __importDefault(require("./getFolderFiles"));
const transformFilesResponse_1 = __importDefault(require("./transformFilesResponse"));
const mimeTypes = __importStar(require("../config/mimeTypes.json"));
const filesHandler_1 = __importDefault(require("./filesHandler"));
const asyncForEach_1 = __importDefault(require("./asyncForEach"));
/*const defaultConfig: FileConfig = {
  limit: false,
  thumbnail: { width: 200, height: 200, fit: 'inside' },
  copies: false,
  allowed: ['images', 'documents', 'video', 'audio'],
  errors: {
    allowed: 'Tipo de archivo no permitido',
  },
};*/
const uploadDir = (path, destination) => __awaiter(void 0, void 0, void 0, function* () {
    const files = getFolderFiles_1.default(path);
    yield asyncForEach_1.default(files, (file) => __awaiter(void 0, void 0, void 0, function* () {
        yield filesHandler_1.default.upload(file, `${destination}${file.replace(path, '')}`);
    }));
});
const removeDir = (path) => __awaiter(void 0, void 0, void 0, function* () {
    const files = yield filesHandler_1.default.listFilesByPrefix(path, null);
    yield asyncForEach_1.default(files, (file) => __awaiter(void 0, void 0, void 0, function* () {
        yield file.delete();
    }));
});
/**
 * Convierte el array de tipos de archivos permitidos a extensiones
 * @param allowed
 * return string[]
 */
function transformAllowed(allowed) {
    let result = [];
    allowed.forEach((value) => {
        const { abbreviations } = mimeTypes;
        const extensions = abbreviations[value];
        if (extensions) {
            result = [...result, ...extensions];
        }
        if (mimeTypes.types[value]) {
            result.push(value);
        }
    });
    return result.map((ext) => mimeTypes.types[ext]);
}
/**
 * Se sobre escribe la función toJSON para agregar las urls a los archivos
 * @param Model
 */
function overwriteToJSON(Model) {
    // @ts-ignore __oldToJSON no existe en las instancias de los modelos
    Model.prototype.__oldToJSON = Model.prototype.toJSON;
    Model.prototype.toJSON = function toJSON() {
        const data = this.__oldToJSON();
        return transformFilesResponse_1.default(Model, data);
    };
}
/**
 * Genera la ruta del archivo en GCP
 * @param file
 * @param modelName
 * @param id
 * @param attribute
 */
const getDestPath = (file, modelName, id, attribute) => {
    const { tmpDir } = file;
    return `${modelName}-${id}/${attribute}/${tmpDir}`;
};
/**
 * Pone el Hook afterFind llamado 'afterFindFiles' al modelo especificado
 *
 * @param Model Sequelize Model class
 * @paran sequelize Sequelize instance
 * @returns {Sequelize.Model}
 */
function setup(Model) {
    overwriteToJSON(Model);
    // Si es un Modelo de sequelize y tiene el atributo filesConfig
    if (typeof Model.filesConfig === 'object') {
        Object.keys(Model.filesConfig).forEach((field) => {
            const { allowed } = Model.filesConfig[field];
            Model.filesConfig[field].allowed = transformAllowed(allowed);
        });
        // Hook que se encarga de limpiar los datos que se guardaran en los campos tipo archivo
        const beforeCreate = (instance) => __awaiter(this, void 0, void 0, function* () {
            yield asyncForEach_1.default(Object.keys(Model.filesConfig), (attribute) => __awaiter(this, void 0, void 0, function* () {
                const files = instance.get(attribute);
                if (files) {
                    if (Array.isArray(files)) {
                        const cleanedFiles = files.map((file) => {
                            const { mimetype, tmpDir, size, filename, uploadedBy, uploadedAt, id } = file;
                            return { mimetype, tmpDir, size, filename, uploadedAt, uploadedBy, id };
                        });
                        instance.set(attribute, cleanedFiles);
                    }
                    else if (typeof files === 'object') {
                        const { mimetype, tmpDir, size, filename, uploadedAt, uploadedBy, id } = files;
                        instance.set(attribute, {
                            mimetype,
                            tmpDir,
                            size,
                            filename,
                            uploadedAt,
                            uploadedBy,
                            id,
                        });
                    }
                }
            }));
        });
        const beforeUpdate = (instance) => __awaiter(this, void 0, void 0, function* () {
            if (Model.filesConfig) {
                instance.__newFiles = {};
                instance.__deletedFiles = {};
            }
            yield asyncForEach_1.default(Object.keys(Model.filesConfig), (attribute) => __awaiter(this, void 0, void 0, function* () {
                if (instance.changed(attribute)) {
                    const value = instance.get(attribute);
                    const prev = instance.previous(attribute);
                    if (Array.isArray(value) && Array.isArray(prev)) {
                        // Se obtienen los nombres de la carpeta temporal de cada archivo (valor actual y anterior)
                        const curTmpDirs = value.map(file => file.tmpDir);
                        const prevTmpDirs = prev.map(file => file.tmpDir);
                        /*
                          se guarda la info de los archivos nuevos para saber cual archivo se debe subir a GCP
                          en el hook afterUpdate
                          */
                        instance.__newFiles[attribute] = lodash_1.differenceWith(curTmpDirs, prevTmpDirs, lodash_1.isEqual).map(tmpDir => {
                            return value.find(file => file.tmpDir === tmpDir);
                        });
                        /*
                        se guarda la info de los archivos eliminados para poder eliminarlos de GCP en el hook
                        afterUpdate
                        */
                        instance.__deletedFiles[attribute] = lodash_1.differenceWith(prevTmpDirs, curTmpDirs, lodash_1.isEqual).map(tmpDir => {
                            return prev.find(file => file.tmpDir === tmpDir);
                        });
                    }
                    else {
                        // Si el valor anterior esta vacío nos indica que el archivo es nuevo
                        if (lodash_1.isEmpty(prev)) {
                            instance.__newFiles[attribute] = [value];
                        }
                        else if (!Array.isArray(value)
                            && !Array.isArray(prev)
                            && value.tmpDir !== prev.tmpDir) {
                            /*
                            Si el valor anterior no esta vacío y los tmpDir son diferentes, indica que el archivo
                            fue remplazado y hay que eliminar el anterior y subir el nuevo archivo
                             */
                            instance.__newFiles[attribute] = [value];
                            instance.__deletedFiles[attribute] = [prev];
                        }
                    }
                    // Limpia la info que se guardara en el campo, quita los atributos no necesarios
                    if (value) {
                        if (Array.isArray(value)) {
                            instance.set(attribute, value.map(file => {
                                const { mimetype, tmpDir, size, filename, uploadedBy, uploadedAt, } = file;
                                return { mimetype, tmpDir, size, filename, uploadedBy, uploadedAt };
                            }));
                        }
                        else {
                            const { mimetype, tmpDir, size, filename, uploadedBy, uploadedAt, } = value;
                            instance.set(attribute, {
                                mimetype,
                                tmpDir,
                                size,
                                filename,
                                uploadedBy,
                                uploadedAt,
                            });
                        }
                    }
                }
            }));
        });
        // Hook que se encarga de subir los archivos a GCP después de guardar los registros en BD
        const afterCreate = (instance) => __awaiter(this, void 0, void 0, function* () {
            yield asyncForEach_1.default(Object.keys(Model.filesConfig), (attribute) => __awaiter(this, void 0, void 0, function* () {
                const files = instance.get(attribute);
                if (Array.isArray(files)) {
                    yield asyncForEach_1.default(files, (file) => __awaiter(this, void 0, void 0, function* () {
                        const destination = getDestPath(file, Model.name, instance.id, attribute);
                        const path = `${process.env.API_UPLOADS_PATH}/${file.tmpDir}`;
                        yield uploadDir(path, destination);
                    }));
                }
                else if (files && files.tmpDir) {
                    const destination = getDestPath(files, Model.name, instance.id, attribute);
                    const path = `${process.env.API_UPLOADS_PATH}/${files.tmpDir}`;
                    yield uploadDir(path, destination);
                }
            }));
        });
        const afterUpdate = (instance) => __awaiter(this, void 0, void 0, function* () {
            yield asyncForEach_1.default(Object.keys(Model.filesConfig), (attribute) => __awaiter(this, void 0, void 0, function* () {
                const toDeleteFiles = instance.__deletedFiles[attribute];
                const newFiles = instance.__newFiles[attribute];
                // Elimina archivos anteriores
                if (!lodash_1.isEmpty(toDeleteFiles)) {
                    // Se usa Promise.all para eliminar todos en paralelo.
                    const deletedFiles = yield Promise.all(toDeleteFiles.map(file => {
                        return removeDir(getDestPath(file, Model.name, instance.id, attribute));
                    }));
                }
                // Sube archivos nuevos
                if (!lodash_1.isEmpty(newFiles)) {
                    // Se usa Promise.all para subir todos los archivos en paralelo
                    yield Promise.all(newFiles.map(file => {
                        return (() => {
                            const destination = getDestPath(file, Model.name, instance.id, attribute);
                            const path = `${process.env.API_UPLOADS_PATH}/${file.tmpDir}`;
                            return uploadDir(path, destination);
                        })();
                    }));
                }
            }));
        });
        Model.beforeCreate('bcUploadFiles', beforeCreate);
        Model.beforeUpdate('buUploadFiles', beforeUpdate);
        Model.afterCreate('acUploadFiles', afterCreate);
        Model.afterUpdate('auUploadFiles', afterUpdate);
        Model.beforeBulkCreate('bbcUploadFiles', (instances) => {
            return instances.forEach(instance => beforeCreate(instance));
        });
        Model.afterBulkCreate('abcUploadFiles', (instances) => {
            return instances.forEach(instance => afterCreate(instance));
        });
    }
    return Model;
}
/**
 * Agrega funcionalidades a los modelos con campos de archivos
 *
 * @param target {Array<Sequelize.Model>|Sequelize.Model}
 * @returns {Error}
 */
function withFiles(target) {
    if (Array.isArray(target)) {
        target.forEach(Model => {
            setup(Model);
        });
    }
    else {
        setup(target);
    }
}
exports.default = withFiles;
//# sourceMappingURL=withFiles.js.map