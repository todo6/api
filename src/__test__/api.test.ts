import http from 'http';
import SuperTest from 'supertest';
import { Application } from 'express';
import Server from '../Server';

let app: Application;
let agent: SuperTest.SuperTest<SuperTest.Test>;
let headers: http.OutgoingHttpHeaders;

beforeAll(async () => {
  const serverClass = new Server();
  app = await serverClass.initialize();
  agent = SuperTest.agent(app);
  headers = {
    'Content-Type': 'application/json'
  };
});

describe('API Welcome endpoint test', () => {
  test('Validar el endpoint de presentación del api', async () => {
    const response = await agent.get('/').set(headers);
    expect(response.status).toEqual(200);
    expect(response.body.version).toBe('1.0');
  });
});
