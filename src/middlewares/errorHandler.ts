import Boom from '@hapi/boom';
import { NextFunction, Request, Response } from 'express';
import { RequestError } from '../types/RequestError';



// eslint-disable-next-line no-unused-vars,@typescript-eslint/no-unused-vars
export default (
  e: RequestError,
  req: Request,
  res: Response,
  next?: NextFunction,
): Response => {
  if (!req.headers['x-test']) {
    console.log('ErrorHandler', { e });
  }

  if (Boom.isBoom(e)) {
    return res
      .status(e.output.statusCode)
      .json({ ...e.output.payload, data: e.data });
  }


  
  if (process.env.NODE_ENV !== 'production') {
    return res.status(500).json({
      statusCode: 500,
      error: e.name,
      message: e.message,
      stack: e.stack ? e.stack.split('\n') : null,
      sql: e.sql,
    });
  }

  return res.status(500).json({
    statusCode: 500,
    error: 'Internal Server Error',
    message: 'An unknown error occurred on the server',
  });
};
