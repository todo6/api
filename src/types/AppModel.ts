import { Model } from 'sequelize';
// import Models from '../models';
import { FileConfig } from './FileConfig';
import { PaginateOptions } from './PaginateOptions';
import { PagingOptions } from './PagingOptions';
import { Attachment } from './Attachment';

// eslint-disable-next-line import/prefer-default-export
export class AppModel extends Model {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public static pagingOptions: PagingOptions<any>;

  public static associate: (models: any) => void;

  public static filesConfig?: { [field: string]: FileConfig };

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public static associations: any;

  public static paginate: (
    options: PaginateOptions,
  ) => ReturnType<typeof Model.findAndCountAll>;

  readonly id?: number;
  // tslint:disable-next-line: variable-name
  __newFiles?: {
    [attribute: string]: Attachment[];
  };
  // tslint:disable-next-line: variable-name
  __deletedFiles?: {
    [attribute: string]: Attachment[];
  };
}
