import { Application, Express, ParamsDictionary } from 'express';
import { Connection } from 'mongoose';
import { UserInstance } from '../models';
import { Locals } from './Locals';

declare module 'express' {
  interface Application {
    dbConnection: Connection;
  }

  export interface ParamsDictionary {
    [key: string]: string;
  }

  export type ParamsArray = string[];
  export type Params = ParamsDictionary | ParamsArray;

  interface Request<
    P extends Params = ParamsDictionary,
    ResBody = any,
    ReqBody = any
  > extends Express.Request {
    app: Application;
    user: UserInstance;
    readonly locals?: Locals;
    setLocal: (key: keyof Locals, value: any) => void;
  }
}

declare module 'express-serve-static-core' {
  interface Application {
    connection: Connection;
  }

  interface Request extends Express.Request {
    app: Application;
    user: UserInstance;
    readonly locals?: { [key: string]: any };
    setLocal: (key: string, value: any) => void;
  }
}
