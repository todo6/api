"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefreshToken = void 0;
const sequelize_1 = require("sequelize");
const AppModel_1 = require("../types/AppModel");
const _1 = require("./");
class RefreshToken extends AppModel_1.AppModel {
}
exports.RefreshToken = RefreshToken;
function setupModel(sequelize) {
    RefreshToken.init({
        id: {
            type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
        },
        userId: {
            type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
            defaultValue: '',
            validate: {
                notEmpty: { msg: 'El id de usuario es requerido' },
            },
        },
        token: {
            type: sequelize_1.DataTypes.STRING,
            defaultValue: '',
            validate: {
                notEmpty: { msg: 'El campo token no puede quedar vacío' },
            },
        },
    }, {
        sequelize,
        tableName: 'RefreshTokens',
        updatedAt: false,
        paranoid: false,
    });
    RefreshToken.associate = function associate() {
        RefreshToken.belongsTo(_1.User);
    };
    return RefreshToken;
}
exports.default = setupModel;
//# sourceMappingURL=RefreshToken.js.map