/* eslint-disable no-param-reassign */
import { Handler, Request, Response, NextFunction } from 'express';

import errorHandler from '../middlewares/errorHandler';
// import { BaseController } from '../types/BaseController';

type handlerWrapperType = (
  cb: Handler,
  withContext?: boolean
) => (req: Request, res: Response, next: NextFunction) => void;

const handlerWrapper: handlerWrapperType = (
  handler,
  withContext = true
): Handler => async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<Response> => {
  try {
    if (typeof handler !== 'function') throw new Error('No es un handler');
    const { dbConnection } = req.app;
    if (withContext) {
      const context = {
        dbConnection,
        models: dbConnection.models,
        user: req.user
    };
      return handler.call(context, req, res, next);
    }
    return handler(req, res, next);
  } catch (e) {
    return errorHandler(e, req, res, next);
  }
};

export default handlerWrapper;
