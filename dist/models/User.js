"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
const boom_1 = __importDefault(require("@hapi/boom"));
const moment_1 = __importStar(require("moment"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const ms_1 = __importDefault(require("ms"));
const uuid_1 = require("uuid");
const sequelize_1 = require("sequelize");
const AppModel_1 = require("../types/AppModel");
const crypto_1 = require("../utils/crypto");
const _1 = require(".");
class User extends AppModel_1.AppModel {
}
exports.User = User;
function setupModel(sequelize) {
    User.init({
        id: {
            type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
        },
        name: {
            type: sequelize_1.DataTypes.STRING(255),
            defaultValue: '',
            validate: {
                notEmpty: { msg: 'El nombre es requerido' },
            },
        },
        lastName: {
            type: sequelize_1.DataTypes.STRING(255),
            defaultValue: '',
            validate: {
                notEmpty: { msg: 'El apellido es requerido' },
            },
        },
        fullName: {
            type: sequelize_1.DataTypes.VIRTUAL,
            get() {
                return `${this.getDataValue('name')} ${this.getDataValue('lastName')}`;
            },
        },
        email: {
            type: sequelize_1.DataTypes.STRING(255),
            defaultValue: '',
            validate: {
                notEmpty: { msg: 'Escribe un email valido' },
                isEmail: { msg: 'Escribe email valido' },
            },
        },
        password: {
            type: sequelize_1.DataTypes.STRING(50),
            allowNull: true,
            set(val) {
                const password = val || Date.now();
                this.setDataValue('password', crypto_1.generatePassword(password));
            },
        },
        phone: {
            type: sequelize_1.DataTypes.STRING(20),
            validate: {
                // isNumeric:true,
                onlyTenNumbers: (value, next) => {
                    if (value.toString().length !== 10 || Number.isNaN(Number.parseInt(value, 10))) {
                        return next('Escriba un teléfono valido');
                    }
                    return next();
                },
            },
        },
        avatar: sequelize_1.DataTypes.JSON,
        enabled: {
            type: sequelize_1.DataTypes.BOOLEAN,
            defaultValue: false,
        },
        confirmed: {
            type: sequelize_1.DataTypes.BOOLEAN,
            defaultValue: false,
        },
        lastSignIn: {
            type: sequelize_1.DataTypes.DATE,
        },
        updatedBy: {
            type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
        },
        createdBy: {
            type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            defaultValue: '',
            validate: {
                notEmpty: { msg: 'El id del creador es requerído' },
            },
        },
    }, { sequelize, tableName: 'Users' });
    User.associate = function associate() {
        User.hasMany(_1.RefreshToken);
    };
    // Sobre escribe la función toJSON default para quitar el password de la respuesta
    User.prototype.toJSON = function toJSON() {
        const result = this.get();
        delete result.password;
        return result;
    };
    User.login = (email, password) => __awaiter(this, void 0, void 0, function* () {
        const user = yield User.findOne({
            where: { email },
        });
        if (!user)
            throw boom_1.default.conflict('El email ingresado no esta registrado');
        if (!user.enabled)
            throw boom_1.default.conflict('Usuario inactivo');
        if (!user.confirmed) {
            throw boom_1.default.conflict('Primero debe confirmar su correo');
        }
        if (!(yield crypto_1.checkPassword(password, user.password))) {
            throw boom_1.default.conflict('Contraseña incorrecta');
        }
        return user;
    });
    // User.prototype.setLastSignIn = async function setLastSignIn(
    //   lastSignIn: string | moment.Moment,
    // ): ReturnType<typeof User.update> {
    //   return this.update('lastSignIn', lastSignIn, { hooks: false });
    // };
    User.pagingOptions = {
        limit: 30,
        order: [
            ['createdAt', 'DESC'],
            ['id', 'DESC'],
        ],
    };
    User.filesConfig = {
        avatar: {
            title: 'Foto de perfil',
            description: 'Imágenes .jpg, .jpeg y .png',
            limit: 1,
            allowed: ['jpg', 'jpeg', 'png'],
            maxFileSize: 5,
            thumbnail: { width: 200, height: 200, fit: 'cover', position: 'entropy' },
            resizeOptions: {
                width: 1024,
                height: 1024,
                fit: 'cover',
                position: 'entropy',
            },
            copies: {
                big: { width: 500, height: 500, fit: 'cover', position: 'entropy' },
                small: { width: 100, height: 100, fit: 'cover', position: 'entropy' },
            },
            errors: {
                maxFileSize: 'No se permiten imágenes de mas de 5 mb',
                allowed: 'Solo se permiten imágenes jpg, jpeg y png',
            },
        },
    };
    User.generateAuthTokens = (user) => __awaiter(this, void 0, void 0, function* () {
        if (process.env.API_JWT_EXPIRES_IN === undefined) {
            console.warn('No se cargó las variables de entorno');
        }
        const { id, email, name, lastName, createdAt } = user;
        const accessTokenExpiresIn = moment_1.default().add(moment_1.duration(ms_1.default(process.env.API_JWT_EXPIRES_IN)));
        const token = jsonwebtoken_1.default.sign({ id, email, name, lastName, createdAt }, process.env.API_JWT_SECRET, { expiresIn: process.env.API_JWT_EXPIRES_IN });
        const refreshToken = uuid_1.v4();
        const refreshTokenExpiresIn = moment_1.default().add(moment_1.duration(ms_1.default(process.env.API_REFRESH_TOKEN_EXPIRES_IN)));
        yield _1.RefreshToken.create({
            token: refreshToken,
            userId: id,
            expiresIn: refreshTokenExpiresIn,
        });
        return { token, refreshToken, accessTokenExpiresIn, refreshTokenExpiresIn };
    });
    return User;
}
exports.default = setupModel;
//# sourceMappingURL=User.js.map