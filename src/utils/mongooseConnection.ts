import chalk from 'chalk';
import mongoose from 'mongoose';
import sleep from './sleep';


export default async(): Promise<mongoose.Connection> => {
  let time = 1;
  let connectSuccess = false;
  let connection: mongoose.Connection;
  do {
    try {
      const message = await new Promise((resolve, reject) => {
        const host = process.env.MONGO_HOST + '/' + process.env.MONGO_NAME;
        console.log({ host });
        mongoose.connect(host, {
          useCreateIndex: true,
          useNewUrlParser: true,
          useUnifiedTopology: true,
        });
        const db = mongoose.connection;
        connection = db;
        db.on('error', (err: Error) => {
          console.log(
            chalk.red('connection error', err)
          );
          return reject(err);
        });
        db.once('open', async () => {
          console.log(
            chalk.blue('Connection to DB successful')
          );
          return resolve({ mongoose });
        });
      });

      connectSuccess = true;
    } catch (exception) {
      console.log({ exception });
      time += time;
      await sleep(time * 1000);
    }
  } while (!connectSuccess && time < 10);

  return connection;
}
