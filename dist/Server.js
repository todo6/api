"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
const compression_1 = __importDefault(require("compression"));
const cors_1 = __importDefault(require("cors"));
const helmet_1 = __importDefault(require("helmet"));
const body_parser_1 = __importDefault(require("body-parser"));
const path_1 = __importDefault(require("path"));
const chalk_1 = __importDefault(require("chalk"));
const express_asyncify_1 = __importDefault(require("express-asyncify"));
const api_1 = __importDefault(require("./api"));
// import authenticate from './middlewares/authenticate';
// import { UserInstance } from './models/UserModel';
const sequelizeSetup_1 = __importDefault(require("./utils/sequelizeSetup"));
const errorHandler_1 = __importDefault(require("./middlewares/errorHandler"));
const notFoundHandler_1 = __importDefault(require("./middlewares/notFoundHandler"));
// import passportSetup from './utils/passportSetup';
// import joiValidation from './middlewares/joiValidation';
// import Schemas from './schemas';
dotenv_1.default.config();
class Server {
    constructor() {
        this.startDate = Date.now();
        this.app = express_asyncify_1.default(express_1.default());
        this.config();
    }
    /**
     * Pone las configuraciones de express y sus middlewares por default
     */
    config() {
        this.app.use(compression_1.default());
        this.app.use(cors_1.default());
        this.app.use(helmet_1.default());
        // Maneja peticiones application/json
        this.app.use(body_parser_1.default.json());
        // Maneja peticiones application/x-www-form-urlencoded
        this.app.use(body_parser_1.default.urlencoded({ extended: true }));
        // Directorios de archivos estáticos
        this.app.use('/statics', express_1.default.static(path_1.default.resolve('statics')));
        this.app.use('/uploads', express_1.default.static(path_1.default.resolve('uploads')));
        // Agrega un log en cada petición
        // this.app.use((req, res, next) => {
        //   console.log('');
        //   console.log(
        //     chalk.yellow.bgBlack(
        //       `${req.isAuthenticated() ? '[auth]' : ''} ${req.method} - ${
        //         req.originalUrl
        //       } `
        //     )
        //   );
        //   console.log('');
        //   next();
        // });
    }
    /**
     * Pone las rutas del API
     * @param sequelize
     */
    routes(sequelize) {
        return __awaiter(this, void 0, void 0, function* () {
            const router = yield api_1.default(sequelize, [
            // authenticate,
            // joiValidation(Schemas)
            ]);
            this.app.use(process.env.API_PREFIX, router);
            // Error handlers
            this.app.use(errorHandler_1.default);
            this.app.use(notFoundHandler_1.default);
        });
    }
    /**
     * Inicia el server http de express
     */
    serve() {
        const port = process.env.SERVER_PORT || 3000;
        this.app.listen(port, () => {
            console.log(chalk_1.default.black.bgCyanBright(` >> API ready on http://localhost:${port} `), `in ${(Date.now() - this.startDate) / 1000} seconds`);
        });
    }
    /**
     * Se ejecuta cuando un error fatal ocurre
     * @param err
     */
    handleFatalError(err) {
        console.error(`>> Fatal error: ${err.message}`);
        console.error(err.stack);
        // if (this.app.sequelize) {
        //   this.app.sequelize.close();
        // }
        process.exit(1);
    }
    /**
     * Se ejecuta cuando se lanza una excepción y esta no fue manejada
     * @param reason
     * @param promise
     */
    handleRejection(reason, promise) {
        console.log('Unhandled Rejection at:', promise, 'reason:', reason);
        // Application specific logging, throwing an error, or other logic here
    }
    /**
     * Se encarga de inicializar la aplicación
     */
    initialize() {
        return __awaiter(this, void 0, void 0, function* () {
            process.on('uncaughtException', this.handleFatalError);
            process.on('unhandledRejection', this.handleRejection);
            const { sequelize } = yield sequelizeSetup_1.default();
            this.app.sequelize = sequelize;
            // passportSetup();
            yield this.routes(sequelize);
            return this.app;
        });
    }
}
exports.default = Server;
//# sourceMappingURL=Server.js.map