declare module 'express-asyncify' {
  export default function<R>(app: any): R;
}
