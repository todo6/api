import Joi from '@hapi/joi';
import { CustomSchema } from '../types/Schemas';

const editChecklist: CustomSchema = {
  post: {
    body: Joi.object().keys({
      title: Joi.string()
        .required()
        .messages({
          'string.base': '"Nombre" es un campo de tipo texto.',
          'any.required': '"Nombre" es un campo requerido.',
        }),
      description: Joi.string()
        .required()
        .messages({
          'string.base': '"Apellido" es un campo de tipo texto.',
          'any.required': '"Apellido" es un campo requerido.',
        }),
    }),
  },
  patch: {
    body: Joi.object().keys({
      title: Joi.string()
        .optional()
        .allow(null),
      description: Joi.string()
        .optional()
        .allow(null),
      check: Joi.boolean().optional().allow(null),
    }),
  },
};

export default editChecklist;
