import { NextFunction, Request, Response } from 'express';
import passport from 'passport';
import Boom from '@hapi/boom';

export default function authenticate(
  req: Request,
  res: Response,
  next: NextFunction,
): ReturnType<typeof passport.authenticate> {
  return passport.authenticate('jwt', { session: false }, (err, user) => {
    if (err) throw Boom.internal(err);
    if (!user) {
      throw Boom.conflict('No tiene permisos para ejecutar esta acción');
    }
    req.user = user;
    return next();
  })(req, res, next);
}
