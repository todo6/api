import express, { Router } from 'express';
import dotenv from 'dotenv';
import compression from 'compression';
import cors from 'cors';
import helmet from 'helmet';
import bodyParser from 'body-parser';
import path from 'path';
import chalk from 'chalk';
import asyncify from 'express-asyncify';
import { Connection } from 'mongoose';
import api from './api';
import authenticate from './middlewares/authenticate';
import mongooseConnection from './utils/mongooseConnection';
import errorHandler from './middlewares/errorHandler';
import notFoundHandler from './middlewares/notFoundHandler';
import passportSetup from './utils/passportSetup';

dotenv.config();

export default class Server {
  protected app: express.Application;
  protected startDate: number;

  constructor() {
    this.startDate = Date.now();
    this.app = asyncify(express());
    this.config();
  }

  /**
   * Pone las configuraciones de express y sus middlewares por default
   */
  private config(): void {
    this.app.use(compression());
    this.app.use(cors());
    this.app.use(helmet());
    // Maneja peticiones application/json
    this.app.use(bodyParser.json());
    // Maneja peticiones application/x-www-form-urlencoded
    this.app.use(bodyParser.urlencoded({ extended: true }));
    // Directorios de archivos estáticos
    this.app.use('/statics', express.static(path.resolve('statics')));
    this.app.use('/uploads', express.static(path.resolve('uploads')));
  }

  /**
   * Pone las rutas del API
   * @param sequelize
   */
  private async routes(dbConnection: Connection): Promise<void> {
    const router: Router = await api(dbConnection, [
      authenticate,
    ]);
    this.app.use(process.env.API_PREFIX, router);
    // Error handlers
    this.app.use(errorHandler);
    this.app.use(notFoundHandler);
  }

  /**
   * Inicia el server http de express
   */
  public serve(): void {
    const port = process.env.SERVER_PORT || 3000;
    this.app.listen(port, () => {
      console.log(
        chalk.black.bgCyanBright(` >> API ready on http://localhost:${port} `),
        `in ${(Date.now() - this.startDate) / 1000} seconds`
      );
    });
  }

  /**
   * Se ejecuta cuando un error fatal ocurre
   * @param err
   */
  private handleFatalError(err: Error): void {
    console.error(`>> Fatal error: ${err.message}`);
    console.error(err.stack);
    if (this.app && this.app.dbConnection) {
      this.app.dbConnection.close();
    }
    process.exit(1);
  }

  /**
   * Se ejecuta cuando se lanza una excepción y esta no fue manejada
   * @param reason
   * @param promise
   */
  private handleRejection(
    reason: {} | null | undefined,
    promise: Promise<void>
  ): void {
    console.log('Unhandled Rejection at:', promise, 'reason:', reason);
    // Application specific logging, throwing an error, or other logic here
  }

  /**
   * Da inicio a la aplicación
   */
  public async initialize(): Promise<express.Application> {
    process.on('uncaughtException', this.handleFatalError);
    process.on('unhandledRejection', this.handleRejection);
    const dbConnection = await mongooseConnection();
    this.app.dbConnection = dbConnection;
    passportSetup();
    await this.routes(dbConnection);
    return this.app;
  }
}
