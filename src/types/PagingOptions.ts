export interface PagingOptions<M = object> {
  limit: number;
  order: [keyof M, 'asc' | 'ASC' | 'desc' | 'DESC'][];
}