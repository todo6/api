import { Connection } from 'mongoose';

// eslint-disable-next-line import/prefer-default-export
export class BaseController {
  public models: any;
  public dbConnection: Connection;

  constructor(dbConnection: Connection) {
    this.models = dbConnection.models;
    this.dbConnection = dbConnection;
  }
}
