import { FindOptions } from 'sequelize';

export interface PaginateOptions extends FindOptions {
  page?: number;
}
