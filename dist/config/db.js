/* eslint-disable global-require  */
const chalk = require('chalk');
const syntax = require('sequelize-log-syntax-colors');
const dotenv = require('dotenv');
dotenv.config();
const dev = process.env.NODE_ENV === 'development';
const log = s => {
    if (!s.includes('SELECT 1+1 AS result')) {
        const query = s.replace('Executed (default): ', '');
        console.log(chalk `{bgYellow.black ${' SQL > '}} {yellowBright ${syntax(query)}}`);
    }
};
module.exports = {
    username: process.env.MYSQL_USER || 'xxx',
    password: process.env.MYSQL_PASSWORD || 'xxx',
    database: process.env.MYSQL_DATABASE || 'xxx',
    port: Number(process.env.MYSQL_PORT) || 3306,
    host: process.env.MYSQL_HOST || 'localhost',
    dialect: 'mysql',
    pool: {
        max: 10,
        min: 0,
        idle: 10000
    },
    define: {
        underscored: false,
        freezeTableName: false,
        charset: 'utf8',
        dialectOptions: {
            collate: 'utf8_general_ci',
            useUTC: true,
            dateString: true,
            typeCast: true
        },
        timestamps: true,
        paranoid: true,
        engine: 'InnoDB'
    },
    seederStorage: 'sequelize',
    logging: dev ? log : false,
    benchmark: true,
    logQueryParameters: dev
};
//# sourceMappingURL=db.js.map