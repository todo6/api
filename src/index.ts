import Server from './Server';

const server = new Server();
server.initialize().then(() => {
  server.serve();
});
