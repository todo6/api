import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import { Strategy as JWTStrategy, ExtractJwt } from 'passport-jwt';
import mongoose from 'mongoose';
import { UserInstance } from '../models/User';
import User from '../models/User';
import dotenv from 'dotenv';
import { userInfo } from 'os';
dotenv.config();

export default function setupStrategies(): void {
  passport.use(
    new LocalStrategy(
      {
        usernameField: 'email',
        passwordField: 'password',
      },
      async (email: string, password: string, done: any) => {
        try {

          const user = await User.schema.methods.login(email, password);
          return done(null, user);
        } catch (err) {
          return done(err);
        }
      },
    ),
  );

  passport.use(
    new JWTStrategy(
      {
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.API_JWT_SECRET,
      },
      async (jwtPayload: { _id: string, email: string }, done: any) => {
        try {
          //TODO: Buscar por _id

          const user = await User.findOne({ email: jwtPayload.email });
          return done(null, user);
        } catch (err) {
          console.log(err);
          return done(err);
        }
      },
    ),
  );
}
