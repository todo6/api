import User, { UserInstance } from '../models/User';

export type Models = {
  User?: typeof User;
};

export type ModelInstance =
  UserInstance;

export default [
  User,
];
