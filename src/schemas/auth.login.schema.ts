import Joi from '@hapi/joi';

import { CustomSchema } from '../types/Schemas';

const loginSchema: CustomSchema = {
  post: {
    body: Joi.object().keys({
      email: Joi.string()
        .email()
        .required(),
      password: Joi.string()
        .required(),
    }),
  },
};

export default loginSchema;
