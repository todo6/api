import { UserInstance } from '../models/User';

export interface Locals {
  user?: UserInstance;
}
