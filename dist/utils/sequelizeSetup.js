"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelizeConnection_1 = __importDefault(require("./sequelizeConnection"));
const Models_1 = __importDefault(require("../types/Models"));
// import withFiles from './withFiles';
function loadModels(sequelize) {
    const initializedModels = {};
    Models_1.default.forEach(model => {
        const ModelClass = model(sequelize);
        initializedModels[ModelClass.name] = ModelClass;
    });
    Object.keys(initializedModels).forEach(modelName => {
        if (initializedModels[modelName].associate) {
            initializedModels[modelName].associate(initializedModels);
        }
    });
    return initializedModels;
}
exports.default = () => __awaiter(void 0, void 0, void 0, function* () {
    const sequelize = yield sequelizeConnection_1.default();
    const loadedModels = loadModels(sequelize);
    // TODO: modulo largo, pendiente agregar
    //   withFiles(Object.values(loadedModels));
    return { sequelize };
});
//# sourceMappingURL=sequelizeSetup.js.map