"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const notFoundHandler = (req, res) => {
    res.status(404).json({
        error: `Cannot ${req.method} ${req.originalUrl}`,
        message: 'La acción no existe'
    });
};
exports.default = notFoundHandler;
//# sourceMappingURL=notFoundHandler.js.map