"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.prepareCipher = exports.decryptCipher = exports.encryptCipher = exports.checkPassword = exports.generatePassword = void 0;
const bcrypt_1 = __importDefault(require("bcrypt"));
const crypto_1 = __importDefault(require("crypto"));
const boom_1 = __importDefault(require("@hapi/boom"));
exports.generatePassword = (password) => {
    const salt = bcrypt_1.default.genSaltSync();
    return bcrypt_1.default.hashSync(password, salt);
};
exports.checkPassword = (candidate, hashedPassword) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return bcrypt_1.default.compareSync(candidate, hashedPassword);
    }
    catch (e) {
        console.log(`Error revisando contraseña: ${e}`);
        throw boom_1.default.notAcceptable(`Error revisando contraseña: ${e}`);
    }
});
exports.encryptCipher = (text) => __awaiter(void 0, void 0, void 0, function* () {
    const { API_SECURITY_CYPHER } = process.env;
    // random initialization vector
    const iv = crypto_1.default.randomBytes(16);
    // random salt
    const salt = crypto_1.default.randomBytes(64);
    // derive encryption key: 32 byte key length
    // in assumption the masterkey is a cryptographic and NOT a password there is no need for
    // a large number of iterations. It may can replaced by HKDF
    // the value of 2145 is randomly chosen!
    const key = crypto_1.default.pbkdf2Sync(API_SECURITY_CYPHER, salt, 2145, 32, 'sha512');
    // AES 256 GCM Mode
    const cipher = crypto_1.default.createCipheriv('aes-256-gcm', key, iv);
    // encrypt the given text
    const encrypted = Buffer.concat([
        cipher.update(text, 'utf8'),
        cipher.final(),
    ]);
    // extract the auth tag
    const tag = cipher.getAuthTag();
    // generate output
    return Buffer.concat([salt, iv, tag, encrypted]).toString('base64');
});
exports.decryptCipher = (cipher) => __awaiter(void 0, void 0, void 0, function* () {
    const { API_SECURITY_CYPHER } = process.env;
    // base64 decoding
    const bData = Buffer.from(cipher, 'base64');
    // convert data to buffers
    const salt = bData.slice(0, 64);
    const iv = bData.slice(64, 80);
    const tag = bData.slice(80, 96);
    const text = bData.slice(96);
    // derive key using; 32 byte key length
    const key = crypto_1.default.pbkdf2Sync(API_SECURITY_CYPHER, salt, 2145, 32, 'sha512');
    // AES 256 GCM Mode
    const decipher = crypto_1.default.createDecipheriv('aes-256-gcm', key, iv);
    decipher.setAuthTag(tag);
    // encrypt the given text
    const decrypted = decipher.update(text, 'binary', 'utf8') + decipher.final('utf8');
    return decrypted;
});
exports.prepareCipher = (param) => __awaiter(void 0, void 0, void 0, function* () {
    const regHashC = new RegExp('hashC=(.*)&hashT');
    const regHashT = new RegExp('hashT=(.*)&hashI');
    const regHashI = new RegExp('hashI=(.*)');
    const contentMatch = param.match(regHashC);
    const tagMatch = param.match(regHashT);
    const ivMatch = param.match(regHashI);
    const cipherObject = {
        content: contentMatch && contentMatch.length >= 1 ? contentMatch[1] : null,
        tag: tagMatch && tagMatch.length >= 1 ? tagMatch[1] : null,
        iv: ivMatch && ivMatch.length >= 1 ? ivMatch[1] : null,
    };
    return cipherObject;
});
//# sourceMappingURL=crypto.js.map