import moment from 'moment';

export interface AuthTokens {
  token: string;
  refreshToken: string;
  accessTokenExpiresIn: moment.Moment;
  refreshTokenExpiresIn: moment.Moment;
}
