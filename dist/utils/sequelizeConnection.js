"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chalk_1 = __importDefault(require("chalk"));
const sequelize_1 = require("sequelize");
const sequelize_log_syntax_colors_1 = __importDefault(require("sequelize-log-syntax-colors"));
const dotenv_1 = __importDefault(require("dotenv"));
const db_1 = __importDefault(require("../config/db"));
dotenv_1.default.config();
// Setup Sequelize Hierarchy
// sequelizeHierarchy(Sequelize);
const dev = process.env.NODE_ENV === 'development';
const log = (s) => {
    if (!s.includes('SELECT 1+1 AS result')) {
        const query = s.replace('Executed (default): ', '');
        console.log(chalk_1.default `{bgYellow.black ${' SQL > '}} {yellowBright ${sequelize_log_syntax_colors_1.default(query)}}`);
    }
};
function sequelizeConnection() {
    return __awaiter(this, void 0, void 0, function* () {
        const dockerHost = 'mysql';
        const envHost = process.env.MYSQL_HOST || 'localhost';
        const config = {
            username: process.env.MYSQL_USER || 'xxx',
            password: process.env.MYSQL_PASSWORD || 'xxx',
            database: process.env.MYSQL_DATABASE || 'xxx',
            port: Number(process.env.MYSQL_PORT) || 3306,
            host: envHost,
            dialect: 'mysql',
            pool: db_1.default.pool,
            define: {
                underscored: false,
                freezeTableName: false,
                charset: 'utf8',
                timestamps: true,
                paranoid: true,
                engine: 'InnoDB'
            },
            logging: db_1.default.logging,
            benchmark: db_1.default.benchmark,
            logQueryParameters: db_1.default.logQueryParameters,
        };
        try {
            const sequelize = new sequelize_1.Sequelize(config);
            yield sequelize.authenticate();
            console.log(chalk_1.default.bold.blue('>> Connection has been established successfully.'));
            return sequelize;
        }
        catch (ex) {
            console.log(chalk_1.default.bold.red('>> Unable to connect to the database:', ex));
        }
        try {
            config.host = dockerHost;
            const sequelize = new sequelize_1.Sequelize(config);
            yield sequelize.authenticate();
            console.log(chalk_1.default.bold.blue('>> Connection has been established successfully.'));
            return sequelize;
        }
        catch (ex) {
            console.log(chalk_1.default.bold.red('>> Unable to connect to the database:', ex));
        }
        // return sequelize
        //   .authenticate()
        //   .then(() => {
        //     console.log(
        //       chalk.bold.blue('>> Connection has been established successfully.'),
        //     );
        //     return sequelize;
        //   })
        //   .catch(() => {
        //     console.error(chalk.red('>> Unable to connect to the database:'));
        //     config.host = dockerHost;
        //     const dockerSequelize = new Sequelize(config);
        //     dockerSequelize.authenticate().then(() => {
        //       return dockerSequelize;
        //     }).catch(() => {
        //       console.error(chalk.red('>> Unable to connect to the database:'));  
        //     });
        //   });
    });
}
exports.default = sequelizeConnection;
//# sourceMappingURL=sequelizeConnection.js.map