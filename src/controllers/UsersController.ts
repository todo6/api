import Boom from '@hapi/boom';
import { Response, Request } from 'express';
import { BaseController } from '../types/BaseController';
import {
  Controller,
  Middleware,
  DisableMiddlewares,
  Get,
  Post,
  Patch,
  Delete,
} from '../types/decorators';
import { Task } from '../models/User';
import userChecklistSchema from '../schemas/users.checklist.schema';
import joiValidation from '../middlewares/joiValidation';

@Controller('users')
export default class UsersController extends BaseController {
  /**
   * @api {get} /users/checklist/
   * @apiName getChecklist
   * @apiDescription
   * Se consultan todas las tareas a realizar o por hacer del usuario,
   * en caso de enviar el campo check por la url se definirá un filtro
   * entre las tareas realizadas o pendientes a realizar.
   * @param req
   * @param res
   */
  @Get('checklist')
  @Middleware([joiValidation(userChecklistSchema.get)])
  @DisableMiddlewares()
  private async getChecklist(req: Request, res: Response): Promise<Response> {
    const { check } = req.query;
    const { checklist } = req.user;
    // si check llega como null doy por hecho que se agregó en la url como true
    if (check !== undefined) {
      const payload = checklist.filter((element: any) => {
        return element.check === (check === 'true' ? true : false );
      });
      return res.json(payload);
    }

    return res.json(checklist);
  }

  /**
   * @api {patch} /users/checklist/:id
   * @apiName editChecklist
   * @apiDescription
   * Se edita una tarea del usuario, se puede
   * editar el titulo, descripción o el campo
   * check que marcar como realizada la
   * tarea o pendiente.
   * @param req
   * @param res
   */
  @Patch('checklist/:id')
  @Middleware([joiValidation(userChecklistSchema.patch)])
  @DisableMiddlewares()
  private async editChecklist(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;
    const { check, title, description } = req.body;
    let checklist = req.user.checklist.find((item) => item._id.toString() === id );
    if (!checklist) {
        throw Boom.notFound('No se encontró la tarea');
    }
    const index = req.user.checklist.indexOf(checklist);
    req.user.checklist[index].check = check || checklist.check;
    req.user.checklist[index].title = title || checklist.title;
    req.user.checklist[index].description = description || checklist.description;
    
    
    req.user.save();
    return res.json({ message: 'Se ha actualizado la tarea', task: req.user.checklist[index] });
  }


  /**
   * @api {delete} /users/checklist/:id
   * @apiName deleteChecklist
   * @apiDescription
   * Se elimina una tarea de maneara definitíva del usuario
   * enviando el id.
   * @param req
   * @param res
   */
  @Delete('checklist/:id')
  @DisableMiddlewares()
  private async deleteChecklist(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;
    let checklist = req.user.checklist.find((item) => item._id.toString() === id);
    if (!checklist) {
      throw Boom.notFound('No se encontró la tarea');
    }
    const index = req.user.checklist.indexOf(checklist);
    req.user.checklist.splice(index,1);
    req.user.save();
    return res.json({ message: 'La tarea fue removida con éxito'});
  }


  /**
   * @api {delete} /users/checklist
   * @apiName deleteChecklist
   * @apiDescription
   * Se eliminan la lista de tareas enviando un filtro.
   * @param req
   * @param res
   */
  @Delete('checklist/check/:check')
  @DisableMiddlewares()
  private async deleteChecklistByFilter(req: Request, res: Response): Promise<Response> {
    const { check } = req.params;
    const newChecklists: Task[] = req.user.checklist.filter((item) => item.check.toString() !== check);
    req.user.checklist = newChecklists;
    req.user.save();
    return res.json({ message: 'Las tareas fueron removidas con éxito'});
  }

  /**
   * @api {post} /users/checklist
   * @apiName addChecklist
   * @apiDescription
   * Se agrega una tarea pendiente por hacer al usuario en sessión
   * @param req
   * @param res
   */
  @Post('checklist')
  @Middleware([joiValidation(userChecklistSchema.post)])
  @DisableMiddlewares()
  private async addChecklist(req: Request, res: Response): Promise<Response> {
    const { title, description } = req.body;
    req.user.checklist.push({ title, description });
    req.user.save();
    const payload = req.user.checklist[req.user.checklist.length -1];
    return res.json({message: 'Se agrega una nueva tarea', task: payload});
  }


  /**
   * @api {get} /users/me
   * @apiName me
   * @apiDescription
   * Se consulta el usuario en sessión, entre los datos se encuentra el checklist
   * @param req
   * @param res
   */
  @Get()
  @DisableMiddlewares()
  me(req: Request, res: Response): Response {
    return res.json(req.user);
  }
}
