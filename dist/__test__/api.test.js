"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const supertest_1 = __importDefault(require("supertest"));
const Server_1 = __importDefault(require("../Server"));
let app;
let agent;
let headers;
beforeAll(() => __awaiter(void 0, void 0, void 0, function* () {
    const serverClass = new Server_1.default();
    app = yield serverClass.initialize();
    agent = supertest_1.default.agent(app);
    headers = {
        'Content-Type': 'application/json'
    };
}));
describe('API Welcome endpoint test', () => {
    test('Validar el endpoint de presentación del api', () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield agent.get('/').set(headers);
        expect(response.status).toEqual(200);
        expect(response.body.version).toBe('1.0');
    }));
});
//# sourceMappingURL=api.test.js.map