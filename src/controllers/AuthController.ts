import { Request, Response } from 'express';
import passport from 'passport';
import { BaseController } from '../types/BaseController';
import {
  Controller,
  DisableGlobalMiddlewares,
  Get,
  Post,
  Middleware,
} from '../types/decorators';
import { generatePassword } from '../utils/crypto';
import User from '../models/User';
import { UserInstance } from '../models/User';
import errorHandler from '../middlewares/errorHandler';
import joiValidation from '../middlewares/joiValidation';
import loginSchema from '../schemas/auth.login.schema';
import signUpSchema from '../schemas/auth.signUp.schema';
import Boom from '@hapi/boom';

@Controller()
class AuthController extends BaseController {
  /**
   * @api {post} /auth/login
   * @apiName login
   * @apiDescription
   * Se envía email y contraseña para validar ingreso al sistema
   * una vez sean correctos los datos el api responderá con el token y los
   * datos del usuario.
   * @param req
   * @param res
   */
  @Post()
  @DisableGlobalMiddlewares()
  @Middleware([joiValidation(loginSchema.post)])
  async login(req: Request, res: Response): Promise<Response> {
    // Se inicia la validación de los datos para el usuario
    return passport.authenticate(
      'local',
      { session: false },
      async (err: Error, user: UserInstance) => {
        // Si regresa un error entonces mostrar al usuario el problema
        if (err) {
          return errorHandler(err, req, res);
        }

        // Si regresa los datos de usuario entonces se responde con el token
        if (user) {
          const session = await user.schema.methods.getSession(user);
          return res.json(session);
        }
        // Si no hay error ni usuario entonces es por una mala contraseña
        return res.status(403).send('Usuario o contraseña incorrecta');
      },
    )(req, res);
  }

  /**
   * @api {post} /auth/sing-up
   * @apiName singUp
   * @apiDescription
   * Aquí los usuario se pueden dar de alta enviando todos los
   * datos, en esta versión de prueba el usuario no ocupa confirmar
   * la dirección de correo.
   * @param req
   * @param res
   */
  @Post()
  @DisableGlobalMiddlewares()
  @Middleware([joiValidation(signUpSchema.post)])
  async signUp(req: Request, res: Response): Promise<Response> {
      // @ts-ignore
  let { password } = req.body;
  if (password) {
    let val = password || Date.now();
    // @ts-ignore
    password = generatePassword(val);
  }
    const user = new User({
      ...req.body,
      password,
      confirm: true,
      enabled: true,
    });
    
    user.save( (err: any) => {
      console.log(err);
      if (err) {
        throw Boom.badData('Ocurrió un error de validación', err.errors);
      }
    });
    return res.json({ user, message: 'Usuario creado con éxito' });
  }
}

export default AuthController;
