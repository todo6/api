import SuperTest from 'supertest';
import faker from 'faker';
import { Application } from 'express';
import Server from '../../Server';
import { Session } from '../../types/Session';

let app: Application;
let agent: SuperTest.SuperTest<SuperTest.Test>;

beforeAll(async () => {
  const serverClass = new Server();
  app = await serverClass.initialize();
  agent = SuperTest.agent(app.listen());
});

let session: Session;
let header = {
  'Content-Type': 'application/json',
  'x-test': 'true',
  'Authorization': 'bearer ',
};

const userFake =  {
  password: '12345',
  name: faker.name.firstName(),
  lastName: faker.name.lastName(),
  email: faker.internet.email(),
};

describe('Test general de sistema', () => {


    test('Registro de usuario', async () => {
      const response = await agent.post('/auth/sign-up').send(userFake);
      expect(200).toBe(response.status);
      expect(userFake.name).toBe(response.body.user.name);
    });

    test('Hacer login del usuario registrado', async () => {
      const response = await agent.post('/auth/login').set(header).send({
        email: userFake.email,
        password: userFake.password,
      });
      expect(200).toBe(response.status);
      session = response.body;
      header.Authorization += session.token;
    });
  
    test('Comprobar que tengo mi sessión', async () => {
      const response = await agent.get('/users/me').set(header);
      expect(200).toBe(response.status);
      expect(session.user.name).toBe(response.body.name);
    });

    test('Consultar todas mis tareas, como el usuario es nuevo entonces debo tener 0 tareas', async () => {
      const response = await agent.get('/users/checklist').set(header);
      expect(200).toBe(response.status);
      expect(0).toBe(response.body.length);
    });
  
    test('Agregar una tarea nueva', async () => {
      const checklistFake = {
        title: faker.hacker.verb(),
        description: faker.commerce.department()
      };
      const response = await agent.post('/users/checklist')
        .set(header)
        .send(checklistFake);
      expect(200).toBe(response.status);
      expect(response.body.task.title).toBe(checklistFake.title);
      expect(response.body.task.description).toBe(checklistFake.description);
      expect(response.body.task.check).toBe(false);
      session.user.checklist.push(response.body.task);
    });

    test('Consultar todas mis tareas, anteriormente se agregó una tarea', async () => {
      const response = await agent.get('/users/checklist').set(header);
      expect(200).toBe(response.status);
      expect(1).toBe(response.body.length);
    });
    
    test('Consultar todas mis tareas pendientes, mi única tarea pendiente', async () => {
      const response = await agent.get('/users/checklist?check=false').set(header);
      expect(200).toBe(response.status);
      expect(1).toBe(response.body.length);
    });

    test('Consultar todas mis tareas pendientes finalizadas, aún no finalizo ninguna tarea', async () => {
      const response = await agent.get('/users/checklist?check=true').set(header);
      expect(200).toBe(response.status);
      expect(0).toBe(response.body.length);
    });

    test('Edito la descripción', async () => {
      const [item] = session.user.checklist;

      const title = faker.commerce.productName();
      const response = await agent.patch(`/users/checklist/${item._id}`).set(header).send({
        title
      });
      expect(200).toBe(response.status);

      expect(title).toBe(response.body.task.title);
    });


    test('Se marca mi única tarea como realizada', async () => {
      const [item] = session.user.checklist;
      const response = await agent.patch(`/users/checklist/${item._id}`).set(header).send({
        check: true
      });
      expect(200).toBe(response.status);
      expect(true).toBe(response.body.task.check);
    });


    test('Consultar todas mis tareas pendientes, ya no tengo tareas pendientes', async () => {
        const response = await agent.get('/users/checklist?check=false').set(header);
        expect(200).toBe(response.status);
        expect(0).toBe(response.body.length);
    });
  
    test('Consultar todas mis tareas pendientes finalizadas, solo tengo 1 tarea finalizada', async () => {
      const response = await agent.get('/users/checklist?check=true').set(header);
      expect(200).toBe(response.status);
      expect(1).toBe(response.body.length);
    });

    test('Elimino la tarea realizada', async () => {
        const [item] = session.user.checklist;
        const response = await agent.delete(`/users/checklist/${item._id}`).set(header);
        expect(200).toBe(response.status);
    });

    test('Consultar todas mis tareas, no debo tener tareas porque la eliminé', async () => {
      const response = await agent.get('/users/checklist').set(header);
      expect(200).toBe(response.status);
      expect(0).toBe(response.body.length);
      session.user.checklist= [];
    });


    test('Agregar una tarea nueva', async () => {
      const checklistFake = {
        title: faker.hacker.verb(),
        description: faker.commerce.department()
      };
      const response = await agent.post('/users/checklist')
        .set(header)
        .send(checklistFake);
      expect(200).toBe(response.status);
      expect(response.body.task.title).toBe(checklistFake.title);
      expect(response.body.task.description).toBe(checklistFake.description);
      expect(response.body.task.check).toBe(false);
      session.user.checklist.push(response.body.task);
    });
    test('Se marca mi única tarea como realizada', async () => {
      const [item] = session.user.checklist;
      const response = await agent.patch(`/users/checklist/${item._id}`).set(header).send({
        check: true
      });
      expect(200).toBe(response.status);
      expect(true).toBe(response.body.task.check);
    });

    test('Eliminan todas mis tareas realizadas', async () => {
      const response = await agent.delete(`/users/checklist?check=true`).set(header);
      expect(200).toBe(response.status);
  });
});