import Joi from '@hapi/joi';
import { CustomSchema } from '../types/Schemas';

const userObject = Joi.object().keys({
    name: Joi.string()
      .required()
      .messages({
        'string.base': '"Nombre" es un campo de típo texto.',
        'any.required': '"Nombre" es un campo requerido.',
      }),
    lastName: Joi.string()
      .required()
      .messages({
        'string.base': '"Apellido" es un campo de típo texto.',
        'any.required': '"Apellido" es un campo requerido.',
      }),
    email: Joi.string()
      .email()
      .required()
      .messages({
        'string.base': '"Email" es un campo de típo texto.',
        'string.email': '"Email" no es típo email',
      }),
    password: Joi.string()
    .required()
    .messages({
        'string.base': '"password" es un campo de típo texto',
        'any.required': '"password" es un campo requerido.',
      }),
  });

const signUpSchema: CustomSchema = {
  post: {
    body: userObject,
  },
};

export default signUpSchema;
