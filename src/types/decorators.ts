import { Handler, RouterOptions } from 'express';
import { get, hasIn } from 'lodash';
import 'reflect-metadata';

import camelToKebab from '../utils/camelToKebab';

export type RouterPropertiesType = {
  httpVerb: 'get' | 'post' | 'patch' | 'put' | 'delete';
  routeMiddleware?: Handler | Handler[];
  path: string;
  disableGlobalMiddlewares?: boolean;
  disableControllerMiddlewares?: boolean;
};

const defaultRouterProperties: RouterPropertiesType = {
  httpVerb: 'get',
  path: '',
  disableControllerMiddlewares: false,
  disableGlobalMiddlewares: false,
  routeMiddleware: [],
};

const defaultActionsPath = {
  index: '',
  add: '',
  show: ':id',
  edit: ':id',
  delete: ':id',
  restore: 'restore/:id',
  destroy: 'destroy/:id',
  trashed: 'trashed',
};

function getDefaultActionPath(action: string): string {
  return hasIn(defaultActionsPath, action)
    ? get(defaultActionsPath, action)
    : camelToKebab(action);
}

function helperForRoutes(
  httpVerb: RouterPropertiesType['httpVerb'],
  path?: string,
): MethodDecorator {
  return (
    target: any,
    propertyKey: string | symbol,
    descriptor?: PropertyDescriptor,
  ): PropertyDescriptor | void => {
    let routeProperties: Partial<RouterPropertiesType> = Reflect.getOwnMetadata(
      propertyKey,
      target,
    );
    if (!routeProperties) {
      routeProperties = defaultRouterProperties;
    }
    routeProperties = {
      ...routeProperties,
      httpVerb,
      path:
        path !== undefined
          ? `/${path}`
          : `/${getDefaultActionPath(propertyKey as string)}`,
    };
    Reflect.defineMetadata(propertyKey, routeProperties, target);
    if (descriptor) {
      return descriptor;
    }
    return undefined;
  };
}

export function Get(path?: string): MethodDecorator {
  return helperForRoutes('get', path);
}

export function Post(path?: string): MethodDecorator {
  return helperForRoutes('post', path);
}

export function Put(path?: string): MethodDecorator {
  return helperForRoutes('put', path);
}

export function Patch(path?: string): MethodDecorator {
  return helperForRoutes('patch', path);
}

export function Delete(path?: string): MethodDecorator {
  return helperForRoutes('delete', path);
}

// Class Decorators
export enum ClassKeys {
  BasePath = 'BASE_PATH',
  // eslint-disable-next-line no-shadow
  Middleware = 'MIDDLEWARE',
  Options = 'OPTIONS'
}

export function Controller(path?: string): ClassDecorator {
  return <TFunction extends Function>(target: TFunction): TFunction => {
    const controllerPath = `/${path || camelToKebab(target.name.replace('Controller', ''))}`;
    Reflect.defineMetadata(
      ClassKeys.BasePath,
      controllerPath,
      target.prototype,
    );
    return target;
  };
}

export function ControllerMiddleware(
  middleware: Handler | Handler[],
): ClassDecorator {
  return <TFunction extends Function>(target: TFunction): TFunction => {
    Reflect.defineMetadata(ClassKeys.Middleware, middleware, target.prototype);
    return target;
  };
}

export function ControllerOptions(options: RouterOptions): ClassDecorator {
  return <TFunction extends Function>(target: TFunction): TFunction => {
    Reflect.defineMetadata(ClassKeys.Options, options, target.prototype);
    return target;
  };
}

export function Middleware(middleware: Handler | Handler[]): MethodDecorator {
  return (
    target: any,
    propertyKey?: string | symbol,
    descriptor?: PropertyDescriptor,
  ): PropertyDescriptor | void => {
    let routeProperties = Reflect.getOwnMetadata(
      propertyKey,
      target,
    ) as RouterPropertiesType;
    if (!routeProperties) {
      routeProperties = defaultRouterProperties;
    }
    routeProperties.routeMiddleware = middleware;
    Reflect.defineMetadata(propertyKey, routeProperties, target);
    // For class methods that are not arrow functions
    if (descriptor) {
      return descriptor;
    }
    return undefined;
  };
}

export function DisableGlobalMiddlewares(): MethodDecorator {
  return (
    target: any,
    propertyKey?: string | symbol,
    descriptor?: PropertyDescriptor,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any,consistent-return
  ): PropertyDescriptor | void => {
    const routeProperties = Reflect.getOwnMetadata(propertyKey, target) || {};
    routeProperties.disableGlobalMiddlewares = true;
    Reflect.defineMetadata(propertyKey, routeProperties, target);

    if (descriptor) {
      return descriptor;
    }
  };
}

export function DisableControllerMiddlewares(): MethodDecorator {
  return (
    target: any,
    propertyKey?: string | symbol,
    descriptor?: PropertyDescriptor,
  ): PropertyDescriptor | void => {
    const routeProperties = Reflect.getOwnMetadata(propertyKey, target) || {};
    routeProperties.disableControllerMiddlewares = true;
    Reflect.defineMetadata(propertyKey, routeProperties, target);

    if (descriptor) {
      return descriptor;
    }
    return undefined;
  };
}

export function DisableMiddlewares(): MethodDecorator {
  // eslint-disable-next-line consistent-return,@typescript-eslint/no-explicit-any
  return (
    target: any,
    propertyKey?: string | symbol,
    descriptor?: PropertyDescriptor,
  ): PropertyDescriptor | void => {
    const routeProperties = Reflect.getOwnMetadata(propertyKey, target) || {};
    routeProperties.disableGlobalMiddleware = true;
    routeProperties.disableControllerMiddleware = true;
    Reflect.defineMetadata(propertyKey, routeProperties, target);

    if (descriptor) {
      return descriptor;
    }
    return undefined;
  };
}
