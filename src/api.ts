import { Router, Handler } from 'express';
import asyncify from 'express-asyncify';
import { Connection } from 'mongoose';
import 'reflect-metadata';
import ctrls from './controllers';
import handlerWrapper from './utils/handlerWrapper';
import controllersToRoutes from './utils/controllersToRoutes';

const router = asyncify<Router>(Router());

export default async function api(
  dbConnection: Connection,
  globalMiddlewares: Handler[] = [],
): Promise<Router> {
  controllersToRoutes(router, globalMiddlewares, dbConnection);


  // WELCOME REQUEST
  router.get(
    '/',
    handlerWrapper(async function action(req, res) {
      const dev = process.env.NODE_ENV === 'development';
      const env = dev ? { env: process.env } : {};
      const routesList = router.stack.map(item => {
        return `${item.route.stack[0].method.toUpperCase()} ${item.route.path}`;
      });
      const routes = dev ? { routes: routesList } : {};
      const controllersName = ctrls.map(controller => controller.name);
      const controllers = dev ? { controllers: controllersName } : {};
      const models = dev ? { models: Object.keys(this.models) } : {};
      const user = dev ? { user: req.user } : {};
      res.send({
        version: '1.0',
        message: 'Welcome ToDo API',
        ...user,
        ...controllers,
        ...models,
        ...routes,
        ...env,
      });
    }),
  );

  return router;
}
