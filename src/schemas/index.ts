import users from './users.checklist.schema';
import authLogin from './auth.login.schema';
import authSignUp from './auth.signUp.schema';

/**
 * Example of desired schema structure
 * const schemas = {
 *      users: {
 *          post: JoiObject,
 *          put: JoiObject,
 *          delete: JoiObject
 *      }
 * };
 */

export default {
  ...users,
}



