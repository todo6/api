"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const express_asyncify_1 = __importDefault(require("express-asyncify"));
require("reflect-metadata");
// import ctrls from './controllers';
// import { RequestError } from './types/RequestError';
const handlerWrapper_1 = __importDefault(require("./utils/handlerWrapper"));
// import controllersToRoutes from './utils/controllersToRoutes';
// import requester from './utils/requester';
const router = express_asyncify_1.default(express_1.Router());
function api(sequelize, globalMiddlewares = []) {
    return __awaiter(this, void 0, void 0, function* () {
        //   controllersToRoutes(router, globalMiddlewares, sequelize);
        // WELCOME REQUEST
        router.get('/', handlerWrapper_1.default(function action(req, res) {
            return __awaiter(this, void 0, void 0, function* () {
                const dev = process.env.NODE_ENV === 'development';
                const env = dev ? { env: process.env } : {};
                const routesList = router.stack.map(item => {
                    return `${item.route.stack[0].method.toUpperCase()} ${item.route.path}`;
                });
                const routes = dev ? { routes: routesList } : {};
                //   const controllersName = ctrls.map(controller => controller.name);
                //   const controllers = dev ? { controllers: controllersName } : {};
                //   const models = dev ? { models: Object.keys(this.models) } : {};
                let filesFields = {};
                if (dev) {
                    Object.keys(this.models).forEach(model => {
                        if (this.models[model].filesConfig) {
                            filesFields[model] = Object.keys(this.models[model].filesConfig);
                        }
                    });
                    filesFields = { filesFields };
                }
                //   const user = dev ? { user: req.user } : {};
                res.send(Object.assign(Object.assign(Object.assign({ version: '1.0', message: 'hello' }, filesFields), routes), env));
            });
        }));
        return router;
    });
}
exports.default = api;
//# sourceMappingURL=api.js.map